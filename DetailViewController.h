//
//  DetailViewController.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 01/04/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageVw;

@property (weak, nonatomic) IBOutlet UILabel *dateAndTime;

@property (weak, nonatomic) IBOutlet UITextView *detailText;

@property NSString *date, *details, *imageStr,*locationStr;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottonView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContrsint;

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property NSString * imageName;

@end
