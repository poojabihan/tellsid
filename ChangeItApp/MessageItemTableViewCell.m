//
//  MessageItemTableViewCell.m
//  Tell Sid
//
//  Created by Tarun Sharma on 25/10/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessageItemTableViewCell.h"

@implementation MessageItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self shadowToView:_view];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) shadowToView:(UIView *)currentView
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 10.0;
    
    currentView.layer.shadowRadius  = 10.0f;
    currentView.layer.shadowColor   = [UIColor grayColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}

@end
