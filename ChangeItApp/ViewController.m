//
//  ViewController.m
//  ChangeItApp
//
//  Created by Tarun Sharma on 21/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ViewController.h"
#import "HistoryViewController.h"
#import "Reachability.h"
#import "MessagesViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "ChatViewController.h"
#import "ChatInTellSid+CoreDataProperties.h"
#import "MessagesInTellSid+CoreDataProperties.h"
#import "HistoryInTellSid+CoreDataProperties.h"
#import "constant.h"
#import "SCLAlertView.h"
#define MAX_HEIGHT 100

@interface ViewController ()
{
    NSString * messageInTVStr;
    UIImage *chosenImage;
    UITextView  *commentTxtView1;
    NSString *longitude, *latitude;
    MBProgressHUD *hud ,* pleaseWaitHud,*registeringHUD;
    NSString *imgString;
    NSArray * dictArray,* inboxDictArray, * itemChatArray,*resultArray,*questionDataArray;
    NSString * inboxStr;
    SCLAlertView *customAlert;
    UITextView * textViewInAlert;

}
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
NSManagedObjectContext * messageContextTS,* chatContextTS ;

@implementation ViewController

#pragma mark ViewLifecycle Delegates

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.delegate=self;
    
    self.messageButton.layer.cornerRadius = 8;
    self.messageButton.clipsToBounds = YES;
    
    self.historyButton.layer.cornerRadius = 8;
    self.historyButton.clipsToBounds = YES;
    
    
    
    self.sendButton.layer.cornerRadius = 10;
    self.sendButton.clipsToBounds = YES;
    
    self.takeAndAddButtonInstance.layer.cornerRadius = 10;
    self.takeAndAddButtonInstance.clipsToBounds = YES;
    
    [self shadowToView:_view1];
    [self shadowToView:_view2];
    [self shadowToView:_view3];
    
    [self shadowToButton:_messageButton];
    [self shadowToButton:_historyButton];
    [self shadowToButton:_sendButton];

    self.commentTxtView.delegate = self;
    
    [self.commentTxtView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    self.commentTxtView.layer.cornerRadius = 5.0;
    [self.commentTxtView.layer setBorderWidth:1.0f];
    [self.commentTxtView setKeyboardType:UIKeyboardTypeASCIICapable];

    
    self.rghtButton.tag = 0;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
   // historyContextTS=[UIAppDelegate managedObjectContext];
    messageContextTS=[UIAppDelegate managedObjectContext];
    chatContextTS=[UIAppDelegate managedObjectContext];
    
    

    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    // getting an Email String
    NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    
    NSString * isRegistered=[standardUserDefaults stringForKey:@"registered"];
    
    NSLog(@"email id in view did load %@",emailIdString);
    if (emailIdString==NULL && isRegistered==NULL)
    {
        
        registeringHUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
        registeringHUD.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
        //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
        
        // Set the label text.
        registeringHUD.label.text = NSLocalizedString(@"Registering PushNotification...", @"HUD loading title");
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    }
    else if(emailIdString==NULL&&[isRegistered isEqualToString:@"yes"]){
        [self presentingEmailPrompt];
    }
    
}
- (void) shadowToView:(UIView *)currentView
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 15.0;
    
    currentView.layer.shadowRadius  = 10.0f;
    currentView.layer.shadowColor   = [UIColor blackColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}

- (void) shadowToButton:(UIButton *)currentButton
{
    currentButton.layer.shadowRadius  = 5.0f;
    currentButton.layer.shadowColor   = [UIColor colorWithRed:207.0f green:3.0f blue:84.0f alpha:.0].CGColor;
    currentButton.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentButton.layer.shadowOpacity = 0.6f;
    currentButton.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentButton.bounds, shadowInsets)];
    currentButton.layer.shadowPath    = shadowPath1.CGPath;
    
}


- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"ID called in ViewDIDLOad=>%@",[notification object]);
    NSString * instanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSLog(@"String ViewDIDLOad%@",instanceID);
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:@"yes" forKey:@"registered"];
    [standardUserDefaults synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        [registeringHUD hideAnimated:YES];
    });
    [self presentingEmailPrompt];
}
-(void)presentingEmailPrompt{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:kAppNameAlert message: @"Please enter your email address in order to send feedback."preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your E-mail";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        //textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        pleaseWaitHud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
        //pleaseWaitHud.contentColor =khudColour;
        pleaseWaitHud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
        //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
        
        // Set the label text.
        pleaseWaitHud.label.text = NSLocalizedString(@"Please wait...", @"HUD loading title");
        NSArray * textfields = alertController.textFields;
        UITextField * Email = textfields[0];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if(internetStatus != NotReachable)
        {
            
            
            if ([self validateEmailWithString:Email.text]) {
                
                NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                NSString * refreshedToken=[[FIRInstanceID instanceID]token];
                
                NSLog(@"FCMTokenString %@",refreshedToken);
                
                
                //NSString * post=[NSString stringWithFormat:@"email_id_to=%@&fcm_token=%@&app_name=%@&device_id=%@&ssecrete=%@",Email.text,refreshedToken,kAppNameAPI,[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"tellsid@1"];
                
                NSDictionary *dictParam = @{@"email_id_to":Email.text,@"fcm_token":refreshedToken?:@"",@"app_name":kAppNameAPI,@"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"ssecrete":@"tellsid@1"};
                
                self.emailURL=[NSString stringWithFormat:@"%@signup/",kBaseURL];
                [Webservice requestPostUrl:self.emailURL parameters:dictParam success:^(NSDictionary *response) {
                    NSLog(@"Response : %@",response);
                    if ([[response objectForKey:@"response"]isEqualToString:@"Succ"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [pleaseWaitHud hideAnimated:YES];
                            [standardUserDefaults setObject:[response objectForKey:@"email"] forKey:@"emailId"];
                            [standardUserDefaults synchronize];
                            
                            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                            
                            // Set the custom view mode to show any view.
                            hud.mode = MBProgressHUDModeCustomView;
                            // Set an image view with a checkmark.
                            UIImage *image = [[UIImage imageNamed:@""] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                            hud.customView = [[UIImageView alloc] initWithImage:image];
                            // Looks a bit nicer if we make it square.
                            hud.square = YES;
                            // Optional label text.
                            hud.label.text = NSLocalizedString(@"Done", @"HUD done title");
                            
                            [hud hideAnimated:YES afterDelay:1.5f];
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                        /// NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
                        [pleaseWaitHud hideAnimated:YES];
                        [standardUserDefaults removeObjectForKey:@"emailId"];
                        [[NSUserDefaults standardUserDefaults]synchronize ];
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        NSLog(@"response is null");
                    });
                        
                    }
                    
                    
                } failure:^(NSError *error) {
                    NSLog(@"Error : %@",error);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [pleaseWaitHud hideAnimated:YES];
                        /// NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
                        [standardUserDefaults removeObjectForKey:@"emailId"];
                        [[NSUserDefaults standardUserDefaults]synchronize ];
                        
                        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                            [self presentViewController:alertController animated:YES completion:nil];
                            
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        NSLog(@"response is null");
                    });

                }];
                
                
                
                
                
                
            }else if ([Email.text isEqualToString:@""]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [pleaseWaitHud hideAnimated:YES];
                });
                UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"Email Address is Required"  preferredStyle:UIAlertControllerStyleAlert];
                
                [alertError addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                           
                                           [self presentViewController:alertController animated:YES completion:nil];
                                           
                                       }]];
                
                [self presentViewController:alertError animated:YES completion:nil];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [pleaseWaitHud hideAnimated:YES];
                });
                UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:@"Email Address is invalid"  message:@"Enter your Valid E-mail"  preferredStyle:UIAlertControllerStyleAlert];
                
                [alertError addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                       {
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                           
                                           [self presentViewController:alertController animated:YES completion:nil];
                                           
                                       }]];
                
                [self presentViewController:alertError animated:YES completion:nil];
                
                
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [pleaseWaitHud hideAnimated:YES];
            });
            UIAlertController *alertError = [UIAlertController  alertControllerWithTitle:kAppNameAlert  message:@"The Internet connection appears to be offline."  preferredStyle:UIAlertControllerStyleAlert];
            
            [alertError addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       
                                       [self presentViewController:alertController animated:YES completion:nil];
                                       
                                   }]];
            
            [self presentViewController:alertError animated:YES completion:nil];
            
            
        }
        
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
         [self presentViewController:alertController animated:YES completion:nil];
    });
   
    
}

-(void)viewDidAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [hud hideAnimated:YES];
    });
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        [self getQuessionarieMethod];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [hud hideAnimated:YES];
        });
    });
    
    NSLog(@"string name before empty %@",self.stringToCheckVC);
    if ([self.stringToCheckVC isEqualToString:@"Messages"])
    {
        //[NSThread detachNewThreadSelector:@selector(historyLoad) toTarget:self withObject:nil];
        self.stringToCheckVC=@"";
        
    }
    else if ([self.stringToCheckVC isEqualToString:@"History"])
    {
        //[NSThread detachNewThreadSelector:@selector(inboxLoad) toTarget:self withObject:nil];
        [self performSelectorOnMainThread:@selector(inboxLoad) withObject:nil waitUntilDone:YES];
        

        self.stringToCheckVC=@"";
        
    }
    else if ([self.stringToCheckVC isEqualToString:@"Chat"])
    {
        //[NSThread detachNewThreadSelector:@selector(inboxLoad) toTarget:self withObject:nil];
        [self performSelectorOnMainThread:@selector(inboxLoad) withObject:nil waitUntilDone:YES];
        self.stringToCheckVC=@"";
        
    }
    NSLog(@"string name after empty %@",self.stringToCheckVC);
    
}

-(void)viewWillAppear:(BOOL)animated{
//    [self.navigationController setNavigationBarHidden:YES];
//    //[indicator stopAnimating];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.view.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    [self.navigationItem setHidesBackButton:YES];

    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}


#pragma mark Navigation method

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    NSLog(@"from VC class %@", [fromVC class]);
    if ([fromVC isKindOfClass:[MessagesViewController class]])
    {
        NSLog(@"Returning from Messages popped controller");
        self.stringToCheckVC=@"Messages";
    }
    else if([fromVC isKindOfClass:[HistoryViewController class]]){
        NSLog(@"Returning from History popped controller");
        self.stringToCheckVC=@"History";
       
        
        
    }
    else if ([fromVC isKindOfClass:[ChatViewController class]]){
        NSLog(@"Returning from chatView ");
        self.stringToCheckVC=@"Chat";
        
    }
    return nil;
}


#pragma mark ScrollView Method

-(void)viewDidLayoutSubviews{
    
    [self.scrollViewInVC setContentSize:CGSizeMake(self.scrollViewInVC.frame.size.width, self.sendButton.frame.origin.y + self.sendButton.frame.size.height + 10)];
    
}

#pragma mark Survey Methods

-(void)getQuessionarieMethod{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        questionDataArray=[[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        if (!(emailIdString==NULL)) {
            NSString * strForQues;
            
            NSLog(@"email id in Appdelgate %@",emailIdString);
            if (emailIdString==NULL) {
                // strForQues = [NSString stringWithFormat:@"%@badgecount?email_id=",kBaseURL];
                strForQues = [NSString stringWithFormat:@"getquestion?email_id=&app_name=%@",kAppNameAPI];
            }
            else{
                strForQues = [NSString stringWithFormat:@"getquestion?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
            }
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            
            [manager GET:strForQues parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSLog(@"response = %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    questionDataArray = [responseObject objectForKey:@"response"];
                    NSLog(@"Response in bool %d ",UIAppDelegate._isQuesFromNotify);
                    if ([[[responseObject objectForKey:@"total"]stringValue] isEqualToString:@"1"]) {
                        UIAppDelegate._isQuesFromNotify=NO;
                        [self showQuestionarrieAlert:[[questionDataArray objectAtIndex:0]objectForKey:@"question"]];
                        
                    }else if (UIAppDelegate._isQuesFromNotify==YES &&[[[responseObject objectForKey:@"total"]stringValue] isEqualToString:@"0"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAppDelegate._isQuesFromNotify=NO;
                            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                            
                            alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ReceivedMessage.caf", [NSBundle mainBundle].resourcePath]];
                            [alert showCustom:self image:[UIImage imageNamed:@"alertIcon.png"] color:khudColour title:kAppNameAlert subTitle:@"You have already submitted your Feedback." closeButtonTitle:@"Done" duration:0.0f];
                        });
                    }
                });
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                NSLog(@"error = %@", error);
                
            }];
        }
    }
}

-(void)showQuestionarrieAlert:(NSString *)question{
    customAlert = [[SCLAlertView alloc] init];
    textViewInAlert=[[UITextView alloc] init];
    //NSString * str1=[[questionDataArray objectAtIndex:0]objectForKey:@"question"];
    question = [question stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    CGRect textRect = [question boundingRectWithSize:CGSizeMake(200, MAX_HEIGHT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil];
    
    CGSize size1 = textRect.size;
    
    [textViewInAlert setFrame:CGRectMake(10, 0, 200, size1.height + 10)];
    textViewInAlert.editable = NO;
    textViewInAlert.selectable = NO;
    textViewInAlert.textAlignment=NSTextAlignmentCenter;
    [textViewInAlert setFont:[UIFont systemFontOfSize:14]];
    
    
    
    
    //textViewInAlert.text=@"ok";
    textViewInAlert.text=question;
    
    [customAlert addCustomView:textViewInAlert];
    
    //UIColor *color = khudColour;
    
    UIView *greenView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 200, 70)];
    //SCLButton *button1 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button1=[UIButton buttonWithType:UIButtonTypeSystem];
    [button1 setBackgroundImage:[UIImage imageNamed:@"smiley.png"] forState:UIControlStateNormal];
    button1.frame=CGRectMake(20, 10, 50, 50);
    [greenView addSubview:button1];
    //SCLButton *button2 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button2=[UIButton buttonWithType:UIButtonTypeSystem];
    [button2 setBackgroundImage:[UIImage imageNamed:@"Neutral.png"] forState:UIControlStateNormal];
    button2.frame=CGRectMake(80, 10, 50, 50);
    [greenView addSubview:button2];
    
    //SCLButton *button3 = [alert addButton:@"" target:self selector:@selector(firstButton)];
    UIButton * button3=[UIButton buttonWithType:UIButtonTypeSystem];
    [button3 setBackgroundImage:[UIImage imageNamed:@"sad.png"] forState:UIControlStateNormal];
    button3.frame=CGRectMake(140, 10, 50, 50);
    [greenView addSubview:button3];
    
    [customAlert addCustomView:greenView];
    
    
    [button1 addTarget:self action:@selector(smileButton) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(neutralButton) forControlEvents:UIControlEventTouchUpInside];
    [button3 addTarget:self action:@selector(sadButton) forControlEvents:UIControlEventTouchUpInside];
    
    SCLButton *button = [customAlert addButton:@"Prefer Not To Answer" target:self selector:@selector(preferNotToAnswerButton)];
    
    button.buttonFormatBlock = ^NSDictionary* (void)
    {
        NSMutableDictionary *buttonConfig = [[NSMutableDictionary alloc] init];
        
        buttonConfig[@"backgroundColor"] = khudColour;
        buttonConfig[@"textColor"] = [UIColor whiteColor];
        //buttonConfig[@"borderWidth"] = @2.0f;
        //buttonConfig[@"borderColor"] = [UIColor greenColor];
        
        return buttonConfig;
    };
    
    NSDate *currentYear=[[NSDate alloc]init];
    currentYear=[NSDate date];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    NSLog(@"Current year is %@",currentYearString);
    
    customAlert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ReceivedMessage.caf", [NSBundle mainBundle].resourcePath]];
    [customAlert showCustom:self image:[UIImage imageNamed:@"alertIcon.png"] color:khudColour title:@"Tell Sid" subTitle:[NSString stringWithFormat:@"%@ © Soft Intelligence Data Centre.",currentYearString] closeButtonTitle:nil duration:0.0f];
    
    // @"2016 © Soft Intelligence Data Centre."
}

- (void)smileButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        //[customAlert hideView];
        [self submitSmileyMethod:@"Happy"];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            });
            [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
            
        
        
        
        
        
    }
    
    
    NSLog(@"SmileButton button tapped");
}
- (void)neutralButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        //[customAlert hideView];
        [self submitSmileyMethod:@"Neutral"];
        
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            });
          [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
        
    }
    
    
    NSLog(@"NeutralButton button tapped");
}
- (void)sadButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[customAlert hideView];
        [self submitSmileyMethod:@"Sad"];
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
             });
            [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
       
        
        
    }
    NSLog(@"SadButton button tapped");
    
}
- (void)preferNotToAnswerButton
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        //[customAlert hideView];
        [self submitSmileyMethod:@"Prefer Not To Answer"];
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            });
            [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
        
        
        
    }
    
    NSLog(@"NotToAnswerButton  tapped");
    
}
-(void)submitSmileyMethod:(NSString *)answer{
    
    NSString *emailIdString;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    NSLog(@"email id %@",emailIdString);
    
    //NSString *post = [NSString stringWithFormat:@"email_id=%@&ques_id=%@&reply=%@",emailIdString,[[questionDataArray objectAtIndex:0] objectForKey:@"ques_id"],answer];
    
    
    self.URLString=[NSString stringWithFormat:@"%@getuseranser/",kBaseURL];
    
    
    //NSURL *baseURL = [NSURL URLWithString:self.URLString];
    
    
    if ([questionDataArray count] > 0)
    {
        
        NSDictionary *dictParam = @{@"email_id":emailIdString,@"ques_id":[[questionDataArray objectAtIndex:0] objectForKey:@"ques_id"],@"reply":answer,@"app_name":kAppNameAPI};
        
        [Webservice requestPostUrl: self.URLString parameters:dictParam success:^(NSDictionary *response) {
            //Success
            NSLog(@"response:%@",response);
            if ([response objectForKey:@"response"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [customAlert hideView];
                    
                    
                    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
                    [alert addButton:@"Done" actionBlock:^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self getQuessionarieMethod];
                        });
                    }];
                    
                    ///[alert showSuccess:kAppNameAlert subTitle:@"Thanks for your Feedback." closeButtonTitle:nil duration:0.0f];
                    [alert showCustom:[UIImage imageNamed:@"alertIcon.png"] color:khudColour title:kAppNameAlert subTitle:@"Thanks for your Feedback." closeButtonTitle:nil duration:0.0f];
                    
                });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    NSLog(@"response is null");
                });
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            }
            
            
            //do code here
        } failure:^(NSError *error) {
            //error
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
                NSLog(@"response is null");
            });
            [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            
        }];
    }
    
}

#pragma mark CoreData Saving methods

//-(void)historyLoad{
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if(internetStatus != NotReachable)
//    {
//        NSLog(@"Called From Navigation Method History Load");
//
//        
//        dictArray = [[NSArray alloc]init];
//        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
//        
//        // getting an NSString
//        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
//        NSLog(@"email id in history %@",emailIdString);
//        if (emailIdString==NULL) {
//            str = [NSString stringWithFormat:@"%@getuserrecord?device_id=%@&app_name=%@&email_id_to=",kBaseURL,[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI];
//        }
//        else{
//            str = [NSString stringWithFormat:@"%@getuserrecord?device_id=%@&app_name=%@&email_id_to=%@",kBaseURL,[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI,emailIdString];
//        }
//
//        NSURL *url = [NSURL URLWithString:str];
//        
//        NSData *urlData = [NSData dataWithContentsOfURL:url];
//        
//        NSError *err;
//        
//        NSMutableDictionary *rootDict = [NSJSONSerialization JSONObjectWithData:urlData options:NSJSONReadingMutableContainers error:&err];
//        
//        dictArray = [rootDict objectForKey:@"response"];
//        //NSLog(@"response %@",rootDict);
//        
//        NSLog(@"Count in Online Service %lu",(unsigned long)dictArray.count);
//        unsigned long i=[self fetchingCoreDataForHistory];
//        NSLog(@"Count %lu",i);
//        
//        if (dictArray.count<i || dictArray.count>i) {
//            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [self deleteAllObjectsInHistory:@"HistoryInTellSid"];
//                [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                    
//                    [historyContextTS performBlockAndWait:^{
//                        HistoryInTellSid * history = [NSEntityDescription insertNewObjectForEntityForName:@"HistoryInTellSid"inManagedObjectContext:historyContextTS];
//                        history.msgdetail = [obj objectForKey:@"msg_detail"];
//                        history.locationdetail = [obj objectForKey:@"location_detail"];
//                        history.createddate = [obj objectForKey:@"created_date"];
//                        history.changeitid = [obj objectForKey:@"changeit_id"];
//                        NSString * Str=[NSString stringWithFormat:@"%@%@",kImageURL,[obj objectForKey:@"images"]];
//                        // NSData *imageData = Str;
//                        history.image=[NSData dataWithContentsOfURL:[NSURL URLWithString:Str]];
//                    }];
//                    
//                    NSError *error;
//                    
//                    if (![historyContextTS save:&error]) {
//                        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//                    }
//                    
//                }];
//                
//            });
//            
//        }
//        
//    }
//    
//    
//}


-(void)inboxLoad{
       Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        

        
        inboxDictArray = [[NSArray alloc]init];
        itemChatArray= [[NSArray alloc]init];
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSLog(@"email id in inbox %@",emailIdString);
        if (emailIdString==NULL) {
            inboxStr= [NSString stringWithFormat:@"getinboxitem?email_id=&app_name=%@",kAppNameAPI];
        }
        else{
            inboxStr = [NSString stringWithFormat:@"getinboxitem?email_id=%@&app_name=%@",emailIdString,kAppNameAPI];
        }
        
        
       
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:inboxStr parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"Response :%@",responseObject);
            if ([responseObject objectForKey:@"response"]) {
                inboxDictArray = [responseObject objectForKey:@"response"];
                
                NSArray * arr;
                BOOL foo = true;
                unsigned long j=[self fetchingCoreDataForMessages];
                
                if ([inboxDictArray isKindOfClass:[NSString class]]) {
                    inboxDictArray = [NSArray new];
                }
                
                for (NSMutableDictionary *dict in inboxDictArray) {
                    NSLog(@"in for %@",[dict objectForKey:@"itemchat"]);
                    arr=[dict objectForKey:@"itemchat"];
                    //NSLog(@"count of item chat in all %lu",(unsigned long)arr.count);
                    unsigned long i=[self fetchingCoreDataForChat:[dict objectForKey:@"changeit_id"]];
                    if (i<arr.count ||i>arr.count|| j<inboxDictArray.count||j>inboxDictArray.count) {
                        foo=false;
                    }
                    
                }
                NSLog(@"wow in VC%d ",foo);
                if (foo==false) {
                    [self deleteAllObjectsInMessage:@"MessagesInTellSid"];
                    [self deleteAllObjectsInChat:@"ChatInTellSid"];
                    [inboxDictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        [messageContextTS performBlockAndWait:^{
                            
                            MessagesInTellSid * inbox = [NSEntityDescription insertNewObjectForEntityForName:@"MessagesInTellSid"inManagedObjectContext:messageContextTS];
                            
                            inbox.lastmsg=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"message"];
                            
                            inbox.msgdetail = [obj objectForKey:@"msg_detail"];
                            inbox.msgtype = [obj objectForKey:@"msg_type"];
                            inbox.createddate=[[[obj objectForKey:@"lastmsg"]objectAtIndex:0]objectForKey:@"created_date"];
                            inbox.changeitid =[obj objectForKey:@"changeit_id"];
                            
                            itemChatArray=[obj objectForKey:@"itemchat"];
                            
                            
                            [itemChatArray enumerateObjectsUsingBlock:^(id  _Nonnull chatObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                
                                if (chatContextTS != nil) {
                                    [chatContextTS performBlockAndWait:^{
                                        
                                        
                                        NSError* error;
                                        
                                        ChatInTellSid * chatCD = [NSEntityDescription insertNewObjectForEntityForName:@"ChatInTellSid"inManagedObjectContext:chatContextTS];
                                        chatCD.changeitid =[obj objectForKey:@"changeit_id"];
                                        chatCD.createddate=[chatObj objectForKey:@"created_date"];
                                        chatCD.message = [chatObj objectForKey:@"message"];
                                        chatCD.posttype=[chatObj objectForKey:@"post_type"];
                                        chatCD.usertype = [chatObj objectForKey:@"user_type"];
                                        if (![chatContextTS save:&error]) {
                                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                        }
                                        
                                        
                                    }];
                                }
                                
                                
                                
                            }];
                            
                            
                            
                            NSError *error;
                            
                            
                            if (![messageContextTS save:&error]) {
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                            }
                        }];
                    }];
                    
                }
                
            }
            else{
                [self deleteAllObjectsInMessage:@"MessagesInTellSid"];
                [self deleteAllObjectsInChat:@"ChatInTellSid"];
            }
          

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error : %@",error);
            [self deleteAllObjectsInMessage:@"MessagesInTellSid"];
            [self deleteAllObjectsInChat:@"ChatInTellSid"];
            
        }];

        
        
    }

}

#pragma mark FetchingCoreData methods

//-(unsigned long)fetchingCoreDataForHistory{
//    __block NSArray * coreDataArray;
//    [historyContextTS performBlockAndWait:^{
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//        NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryInTellSid" inManagedObjectContext:historyContextTS];
//        [fetchRequest setEntity:entity];
//        NSError *error;
//        
//        coreDataArray = [historyContextTS executeFetchRequest:fetchRequest error:&error];
//        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
//    }];
//    return coreDataArray.count;
//}


-(unsigned long)fetchingCoreDataForMessages{
    __block NSArray * coreDataArray;
    [messageContextTS performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessagesInTellSid" inManagedObjectContext:messageContextTS];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        coreDataArray = [messageContextTS executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    return coreDataArray.count;
}

-(unsigned long)fetchingCoreDataForChat:(NSString *)changeID{
    
    
    [chatContextTS performBlockAndWait:^{
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ChatInTellSid" inManagedObjectContext:chatContextTS];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"changeitid == %@", changeID]];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createddate" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        resultArray = [chatContextTS executeFetchRequest:fetchRequest error:&error];
    }];
    return resultArray.count;
    
}

#pragma mark Deleting CoreData methods


//- (void) deleteAllObjectsInHistory: (NSString *) entityDescription  {
//    
//    [historyContextTS performBlockAndWait:^{
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:historyContextTS];
//        [fetchRequest setEntity:entity];
//        
//        NSError *error;
//        NSArray *items = [historyContextTS executeFetchRequest:fetchRequest error:&error];
//        //[fetchRequest release];
//        
//        
//        for (NSManagedObject *managedObject in items) {
//            [historyContextTS deleteObject:managedObject];
//            NSLog(@"%@ object deleted",entityDescription);
//        }
//        if (![historyContextTS save:&error]) {
//            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
//        }
//    }];
//    
//}

- (void) deleteAllObjectsInMessage: (NSString *) entityDescription  {
    [messageContextTS performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:messageContextTS];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [messageContextTS executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        
        for (NSManagedObject *managedObject in items) {
            [messageContextTS deleteObject:managedObject];
            NSLog(@"%@ object deleted",entityDescription);
        }
        if (![messageContextTS save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
    }];
    
}

- (void) deleteAllObjectsInChat: (NSString *) entityDescription  {
    [chatContextTS performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:chatContextTS];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [chatContextTS executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        [chatContextTS performBlockAndWait:^{
            for (NSManagedObject *managedObject in items) {
                [chatContextTS deleteObject:managedObject];
                NSLog(@"%@ object deleted",entityDescription);
            }
            
        }];
        if (![chatContextTS save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
    }];
    
    
}



#pragma mark TextView Delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if((self.commentTxtView.text.length == 0) && (commentTxtView1.text.length == 0))
    {
        _textField1.hidden = false;
        
        self.commentTxtView.backgroundColor = [UIColor clearColor];
        commentTxtView1.backgroundColor = [UIColor clearColor];
    }
    else
    {
        _textField1.hidden = true;
        
        self.commentTxtView.backgroundColor = [UIColor whiteColor];
        commentTxtView1.backgroundColor = [UIColor whiteColor];
    }

    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if((self.commentTxtView.text.length == 0) && (commentTxtView1.text.length == 0))
    {
            _textField1.hidden = false;
        
        self.commentTxtView.backgroundColor = [UIColor clearColor];
        commentTxtView1.backgroundColor = [UIColor clearColor];
    }
    else
    {
        _textField1.hidden = true;
        
        self.commentTxtView.backgroundColor = [UIColor whiteColor];
        commentTxtView1.backgroundColor = [UIColor whiteColor];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark TextField Delegate

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    
    return YES;
}

#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark SendingEmail Methods

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)onSendingMail:(id)sender
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an Email String
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        if (emailIdString==NULL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            
            [self presentingEmailPrompt];
        }
        else{
            messageInTVStr = [self.commentTxtView text];
            
            messageInTVStr = [messageInTVStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [self sendingMail];
        }
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
    }
    
    
}

-(void)sendingMail
{
    if([messageInTVStr length] > 0)
    {
       // [indicator startAnimating];
        
        
        NSString *emailIdString;
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        
        if([self image:self.imageVw.image isEqualTo:[UIImage imageNamed:@"CameraNotAllowed.png"]]==YES){
            imgString=@"";
            
        }
        else{
            NSData *imageData = UIImageJPEGRepresentation(self.imageVw.image, 0.2);
            
            imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
        }
        
        // getting an email NSString
        emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        NSString * refreshedToken=[[FIRInstanceID instanceID]token];
        NSLog(@"email id %@",emailIdString);
        NSLog(@"FCMTokenString %@",refreshedToken);
        
        //NSString *post = [NSString stringWithFormat:@"msg_detail=%@&email_id_to=%@&location_detail=%@&device_id=%@&email_id=%@&images=%@&latitude=%@&longitude=%@&ssecrete=%@&app_name=%@&fcm_token=%@",messageInTVStr,emailIdString,locationText.text,[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"ganesh@chetaru.com",imgString,latitude,longitude,@"tellsid@1",kAppNameAPI,refreshedToken];
        
        
        NSDictionary *dictParam = @{@"msg_detail":messageInTVStr?: @"",@"email_id_to":emailIdString,@"location_detail":self.locationText.text?: @"" ,@"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"images":imgString?: @"",@"latitude":latitude?: @"",@"longitude":longitude?: @"",@"app_name":kAppNameAPI,@"ssecrete":@"tellsid@1",@"fcm_token":refreshedToken?:@""};
        //@"enquiries@thechangeconsultancy.co"
        self.itemPostURL=[NSString stringWithFormat:@"%@postdetails/",kBaseURL];
        [Webservice requestPostUrl:self.itemPostURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Response : %@",response);
            if ([[response objectForKey:@"response"] isEqualToString:@"Succ"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    
                    
                    [self errorAlertWithTitle:kAppNameAlert message:@"Thank you. You'll receive a confirmation within 24 hours." actionTitle:@"Done"];
                    
                    
                    self.commentTxtView.text = @"";
                    
                    self.commentTxtView.backgroundColor = [UIColor clearColor];
                    _textField1.hidden = false;
                    
                    self.locationText.text = @"";
                    self.imageVw.image = [UIImage imageNamed:@"CameraNotAllowed.png"];
                    [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
                    self.deleteButton.hidden=YES;
                    
                });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [hud hideAnimated:YES];
                    
                });
                
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
            }
            
            
            

        } failure:^(NSError *error) {
            NSLog(@"Error : %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [hud hideAnimated:YES];
               });
                
               [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
                
            

            
        }];



}
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        
        
        [self errorAlertWithTitle:kAppNameAlert message:@"Please enter text." actionTitle:@"OK"];
    }
}

- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    if ([data1 isEqualToData:data2]) {
        return YES;
    }else{
        return NO;
    }
    
}
#pragma mark ImagePicker Delegates

- (IBAction)takeAndAddPhoto:(id)sender
{
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
        }
        else
        {
            //[self showingAlert:@"Camera Not Found" :@"This Device has no Camera"];
            
            [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"Dismiss"];
        }
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
            photoPicker.delegate = self;
            photoPicker.allowsEditing = NO;
            photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:photoPicker animated:YES completion:NULL];
        }
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
   
}


-(void)showcamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        //[Helper popAlertMessageWithTitle:@"Error" alertText:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access."];
        
        
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];

    }
    else
    {
        [self camDenied];
    }
    
}

-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];

    
    
    //NSString *alertText;
    //NSString *alertButton;
    
//    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
//    if (canOpenSettings)
//    {
//        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings of this app.\n\n2. Turn the Camera on.";
//        
//        alertButton = @"Go";
//    }
//    else
//    {
//        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
//        
//        alertButton = @"OK";
//    }
//    
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Error"
//                          message:alertText
//                          delegate:self
//                          cancelButtonTitle:alertButton
//                          otherButtonTitles:nil];
//    alert.tag = 3491832;
//    [alert show];
}
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    if (alertView.tag == 3491832)
//    {
//        BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
//        if (canOpenSettings)
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }
//}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    self.imageVw.image = chosenImage;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.imageVw.image, nil, nil, nil);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    self.deleteButton.hidden=NO;
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Location Delegates

- (IBAction)sendLocation:(id)sender
{
   // [indicator startAnimating];
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    if (self.rghtButton.tag == 1)
    {
        [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
        self.rghtButton.tag = 0;
        [self.locationManager stopUpdatingLocation];
        self.locationText.text = @"";
        //[indicator stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        latitude = @"";
        longitude = @"";
    }
    
    else
    {
        self.locationManager = [CLLocationManager new];
        
        self.locationManager.delegate = self;
        
        self.locationManager.distanceFilter = 80.0;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        
        if ([[[UIDevice currentDevice]systemVersion] intValue] >= 8)
        {
            
           if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
                [self.locationManager requestWhenInUseAuthorization];
                
            }
            
        }
        
        [self.locationManager startUpdatingLocation];
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied)
    {
        //location denied, handle accordingly
        NSLog(@"Dont allow");
        //[indicator stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
        self.rghtButton.tag = 0;
        [self.locationManager stopUpdatingLocation];
        self.locationText.text = @"";
        latitude = @"";
        longitude = @"";
        
        
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        NSLog(@"Allow");
        
        //hooray! begin startTracking
    }
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder cancelGeocode];

    [geocoder reverseGeocodeLocation:self.locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
         
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             
             [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
             //[indicator stopAnimating];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [hud hideAnimated:YES];
             });
             [self.locationManager stopUpdatingLocation];
             return;
             
         }
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         CLLocation *location = placemark.location;
         
         longitude = [[NSNumber numberWithDouble: location.coordinate.longitude] stringValue];
         latitude = [[NSNumber numberWithDouble: location.coordinate.latitude] stringValue];
         
         self.locationText.text = [NSString stringWithFormat:@"%@, %@, %@",placemark.locality,placemark.administrativeArea,placemark.country];
         [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
         self.rghtButton.tag = 1;
         
         //[indicator stopAnimating];
         dispatch_async(dispatch_get_main_queue(), ^{
             [hud hideAnimated:YES];
         });
     }];
    
    [self.locationManager stopUpdatingLocation];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error.localizedDescription);
    //[indicator stopAnimating];
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
    [self.locationManager stopUpdatingLocation];
    
    switch([error code])
    {
            
            
        case kCLErrorNetwork: // general, network-related error
        {
            
            
          [self errorAlertWithTitle:kAppNameAlert message:@"Please check your network connection or that you are not in airplane mode." actionTitle:@"OK"];
        }
            break;
        case kCLErrorDenied:{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Background Location Access Disabled" message:@"In order to be notified, please open this app's settings and set location access to 'Always'." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                @try
                {
                    NSLog(@"tapped Settings");
                    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                    if (canOpenSettings)
                    {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        [[UIApplication sharedApplication] openURL:url];
                    }
                }
                @catch (NSException *exception)
                {
                    
                }
                
            }];
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [alert addAction:settingsAction];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
            break;
            
    }
}





#pragma mark HistoryButton Click

- (IBAction)onHistory:(id)sender
{
//    HistoryViewController * historyVC=[self.storyboard instantiateViewControllerWithIdentifier:@"history"];
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//      [self.navigationController pushViewController:historyVC animated:YES];
//    });
    
    

}

#pragma mark DeleteButton Click

- (IBAction)deleteButtonClick:(id)sender {
    self.imageVw.image = [UIImage imageNamed:@"CameraNotAllowed.png"];
    self.deleteButton.hidden=YES;

}



#pragma mark MessageButton Click

- (IBAction)messagesButtonClick:(id)sender {

//    MessagesViewController * messageVC=[self.storyboard instantiateViewControllerWithIdentifier:@"messages"];
//    
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [self.navigationController pushViewController:messageVC animated:YES];
//
//            
//        });
        
}

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
@end
