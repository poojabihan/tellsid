//
//  HistoryInTellSid+CoreDataProperties.m
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "HistoryInTellSid+CoreDataProperties.h"

@implementation HistoryInTellSid (CoreDataProperties)

+ (NSFetchRequest<HistoryInTellSid *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HistoryInTellSid"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic image;
@dynamic locationdetail;
@dynamic msgdetail;

@end
