//
//  ChatInTellSid+CoreDataProperties.h
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatInTellSid+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatInTellSid (CoreDataProperties)

+ (NSFetchRequest<ChatInTellSid *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *changeitid;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *usertype;
@property (nullable, nonatomic, copy) NSString *posttype;

@end

NS_ASSUME_NONNULL_END
