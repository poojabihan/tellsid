//
//  MessagesInTellSid+CoreDataProperties.m
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "MessagesInTellSid+CoreDataProperties.h"

@implementation MessagesInTellSid (CoreDataProperties)

+ (NSFetchRequest<MessagesInTellSid *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessagesInTellSid"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic lastmsg;
@dynamic msgdetail;
@dynamic msgtype;

@end
