//
//  MessagesViewController.h
//  Tell Sid
//
//  Created by Tarun Sharma on 25/10/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;
-(void)loadingElementsInMessage;

@end
