//
//  constant.h
//  Tell Sid
//
//  Created by Tarun Sharma on 04/11/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//demo_tellsid

//Demo new
#define kBaseURL @"https://demo.thechangeconsultancy.co/tellsid/index.php/apitellsid/"
#define kImageURL @"https://demo.thechangeconsultancy.co/tellsid/assets/upload/tellsid/"

#define kChatImageURL @"http://demo.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"

//Live pre
//#define kBaseURL @"http://live.thechangeconsultancy.co/tellsid/index.php/apitellsid/"
//#define kImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/tellsid/"
//#define kChatImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"

//LIVE new

//#define kBaseURL @"http://tellsid.softintelligence.co.uk/index.php/apitellsid/"
//#define kChatImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/chatimg/"
//#define kImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/tellsid/"

#define kAppNameAPI @"tellsid"
#define kAppNameAlert @"Tell Sid"
#define kInternetOff @"Internet Connection Required!"
#define khudColour [UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1]
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#endif /* constant_h */
