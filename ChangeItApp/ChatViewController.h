//
//  ChatViewController.h
//  Tell Sid
//
//  Created by Tarun Sharma on 26/10/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import "Reachability.h"
#import "Webservice.h"
@interface ChatViewController : JSQMessagesViewController<JSQMessagesCollectionViewDataSource,JSQMessagesCollectionViewDelegateFlowLayout,UITextViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property NSString * itemID;

@property NSString * URLString;

@end
