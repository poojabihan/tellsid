//
//  ChatImageViewController.h
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImageViewController : UIViewController<UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property NSString * imageURL;
@property (weak, nonatomic) IBOutlet UIImageView *chatImage;

@end
