//
//  MessagesInTellSid+CoreDataClass.h
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessagesInTellSid : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MessagesInTellSid+CoreDataProperties.h"
