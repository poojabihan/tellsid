//
//  AppDelegate.h
//  ChangeItApp
//
//  Created by Tarun Sharma on 21/03/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>
@import Firebase;
@import FirebaseMessaging;
#import <AFNetworking/AFNetworking.h>
#import "SCLAlertView.h"
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic) BOOL  _isCurrentpageChat;
@property(nonatomic) BOOL  _isQuesFromNotify;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property NSString * urlString;
@property (retain, nonatomic)  Reachability* reach;

@end

