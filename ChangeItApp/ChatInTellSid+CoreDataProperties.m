//
//  ChatInTellSid+CoreDataProperties.m
//  Tell Sid
//
//  Created by Tarun Sharma on 06/12/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import "ChatInTellSid+CoreDataProperties.h"

@implementation ChatInTellSid (CoreDataProperties)

+ (NSFetchRequest<ChatInTellSid *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ChatInTellSid"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic message;
@dynamic usertype;
@dynamic posttype;

@end
