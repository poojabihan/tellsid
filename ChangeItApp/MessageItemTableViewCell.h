//
//  MessageItemTableViewCell.h
//  Tell Sid
//
//  Created by Tarun Sharma on 25/10/16.
//  Copyright © 2016 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleHeadLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *view;

@end
